#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

// Output #owner_menu
void owner_area(user_t *u) {
    int choice;
	do {
        printf("************** OWNER : WELCOME %s **************",u->name);
		printf("\n\n0. Sign Out\n1. Appoint Librarian\n2. Edit Profile\n3. Change Password\n4. Fees/Fine Report\n5. Book Availability\n6. Book Categories/Subjects\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // appoint labrarian
				appoint_librarian();
				break;
			case 2:// Edit Profile
				edit_profile(u);
				break;
			case 3:// Change Password
				change_password(u);
				break;
			case 4:
				break;
			case 5:
				break;
			case 6:
				break;
		}
	}while (choice != 0);
}

// Logic #appoint_librarian
void appoint_librarian() {
	// input #librarian_details
	user_t u;
	user_accept(&u);

	// Logic #labriran_role
	strcpy(u.role, ROLE_LIBRARIAN);

	// Logic add_librarian_into_db
	user_add(&u);
}