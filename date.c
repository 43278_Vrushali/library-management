#include <stdio.h>
#include <time.h>
#include "date.h"

// Input #date
void date_accept(date_t *d) {
	printf("date: ");
	scanf("%d%d%d", &d->day, &d->month, &d->year);
}

// Output #date
void date_print(date_t *d) {
	printf("date: %d-%d-%d\n", d->day, d->month, d->year);
}

// Logic #equating_two_days
int date_equal(date_t d1, date_t d2) {
	if(d1.day == d2.day && d1.month == d2.month && d1.year == d2.year)
		return 1;
	return 0;
}

// Logic #comparing_two_dates
int date_greater(date_t d1, date_t d2) {
	if(d1.year > d2.year || (d1.year == d2.year && d1.month > d2.month) || (d1.year == d2.year && d1.month == d2.month && d1.day > d2.day))
		return 1;
	return 0;
}

// Logic #checking_leap_or_not
int is_leap(int year) {
	if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
		return 1;
	return 0;
}

// Logic #calculating_number_of_days
int month_days(int month, int year) {
	int mon_days[] = { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	if(is_leap(year))
		mon_days[2] = 29;
	return mon_days[month];
}

  // Logic #in_leap_year
  int year_days(int year) {
	 if(is_leap(year))
	 	return 366;
	 return 365;
  }

// Logic #calculating_number_of_days_between_two_dates
int date_cmp(date_t d1, date_t d2) {
	int diff = -1, days = 0;
	date_t min = d1, max = d2;
	
	// Logic #decide_min_max
	if(date_greater(d1, d2)) {
		diff = 1;
		min = d2;
		max = d1;
	}

	// Logic #counting_dates
	while(!date_equal(min, max)) {
		days = days + 1;
		min.day = min.day + 1;

		if(min.day > month_days(min.month, min.year)) {
			min.day = 1;
			min.month = min.month + 1;
		}

		if(min.month > 12) {
			min.day = 1;
			min.month = 1;
			min.year = min.year + 1;
		}
	}

	return diff * days;
}

// Logic #adding_days_in_given_date
date_t date_add(date_t d, int days) {
	date_t res = d;

	while(days > 0) {
		days--;
		res.day = res.day + 1;

		if(res.day > month_days(res.month, res.year)) {
			res.day = 1;
			res.month = res.month + 1;
		}

		if(res.month > 12) {
			res.day = 1;
			res.month = 1;
			res.year = res.year + 1;
		}
	}

	return res;
}

// Logic #taking_current_date
date_t date_current() {

	// Declaration of current function and return current date of function 
	time_t t = time(NULL); //32-bit-long_number

	// Logic #getting_struct_tm by passing &t yo local time
	struct tm *tm = localtime(&t);

	// Logic #seperationg_date_time_taken_from_user
	date_t now;
	now.day = tm->tm_mday;
	now.month = tm->tm_mon + 1;
	now.year = tm->tm_year + 1900;

	return now;
}