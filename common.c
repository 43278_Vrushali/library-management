#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "library.h"

//-----------------------------------------------------
 // Input #user_info
 void user_accept(user_t *u) {
     // printf("id: ");
	 //scanf("%d", &u->id);

	 u->id = get_next_user_id();

	 printf("name: ");
	 scanf("%s", u->name);

	 printf("email: ");
	 scanf("%s", u->email);

	 printf("phone: ");
	 scanf("%s", u->phone);

	 printf("password: ");
	 scanf("%s", u->password);
     
     // Logic #acessing_only 
	 strcpy(u->role, ROLE_MEMBER);
 }

 // Output #user_info
 void user_display(user_t *u) {
     printf("ID : %d\t, NAME : %s\t, EMAIL : %s\t, PHONE : %s\t, ROLE : %s\n", u->id, u->name, u->email, u->phone, u->role);
 }

 // Logic #adiing_user_to_db
 void user_add(user_t *u) {
	// Logic #open_file_for_appending_data
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) {
		perror("failed to open users file");
		return;
	}
	
	// Logic #write_data_into_file
	fwrite(u, sizeof(user_t), 1, fp);
	printf("user added into file.\n");
	
	fclose(fp);
}

// Logic #finding_user_by_email
int user_find_by_email(user_t *u, char email[]) {
	FILE *fp;
	int found = 0;

	// Logic #open_file_for_appending_data
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("failed to open users file");
		return found;
	}

	// Logic #read_users_one_by_one
	while(fread(u, sizeof(user_t), 1, fp) > 0) {
	
		if(strcmp(u->email, email) == 0) {
			found = 1;
			break;
		}
	}
	fclose(fp);

	return found;
}

//------------------------------------------------------------

// Input #book_info
 void book_accept(book_t *b) {
     //printf("id: ");
	 //scanf("%d", &b->id);

	 b->id = get_next_book_id();
     
	 printf("name: ");
	 scanf("%s", b->name);

	 printf("author: ");
	 scanf("%s", b->author);

	 printf("subject: ");
	 scanf("%s", b->subject);

	 printf("price: ");
	 scanf("%lf", &b->price);

	 printf("isbn: ");
	 scanf("%s", b->isbn);
 }

 // Output #book_info
 void book_display(book_t *b) {
     printf("ID : %d\t, NAME : %s\t, AUTHOR : %s\t, SUBJECT : %s\t, PRICE : %.2lf\t, ISBN : %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
 }

//-------------------------------------------------

// Input #book_copy_details
void bookcopy_accept(bookcopy_t *c) {

	//printf("id: ");
	//scanf("%d", &c->id);

	// Logic #autogenrated_bookcopy_id
	c->id = get_next_bookcopy_id();

	printf("book id: ");
	scanf("%d", &c->bookid);

	printf("rack no: ");
	scanf("%d", &c->rack);

	// Logic #acessing_only
	strcpy(c->status, STATUS_AVAIL);
}

// Output #book_info
void bookcopy_display(bookcopy_t *c) {
    printf("ID : %d\t, BOOKID : %d\t, RACK : %d\t, STATUS : %s\n", c->id, c->bookid, c->rack, c->status);
}

//-------------------------------------------------

// Logic #genrating_next_user_id
int get_next_bookcopy_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(bookcopy_t);
	bookcopy_t u;

	// Logic #opening_db
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL)
		return max + 1;
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);
	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Lgic #mx_id
		max = u.id;
	fclose(fp);
	
	return max + 1;
}

//------------------------------------------------------------

// Input #Issue_record_of_books
void issuerecord_accept(issuerecord_t *r) {

	//printf("id: ");
	//scanf("%d", &r->id);

	printf("copy id: ");
	scanf("%d", &r->copyid);

	printf("member id: ");
	scanf("%d", &r->memberid);

	// Logic #entering_backdated_entries
	printf("issue: ");
	date_accept(&r->issue_date);

	// Logic #taking_current_system_date
	//r->issue_date = date_current();

	// Logic #expected_return_date
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);

	// Logic #itialize_zero by memset
	// memset(x,0,y) means from x, y bites are zero
	memset(&r->return_date, 0, sizeof(date_t));

	r->fine_amount = 0.0;
}

// Output #issue_record_of_books
void issuerecord_display(issuerecord_t *r) {
	printf("issue record: %d\tcopy: %d\tmember: %d\tfine: %.2lf\t", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("\treturn due ");
	date_print(&r->return_duedate);
	printf("\treturn ");
	date_print(&r->return_date);
}

// Logic #autogenrate_next_issuerecord_id
int get_next_issuerecord_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;

	// Logic #open_db
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;

	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);
	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Logic #max_id
		max = u.id;
	fclose(fp);
	
	return max + 1;
}

//-----------------------------------------------------------

//Input #payment_acept
void payment_accept(payment_t *p) {

	/*printf("id: ");
	scanf("%d", &p->id);*/
	
	p->id = get_next_payment_id();

	printf("member id: ");
	scanf("%d", &p->memberid);

	printf("type (fees/fine): ");
	scanf("%s", p->type);
	
	strcpy(p->type,PAY_TYPE_FEES);

	printf("amount: ");
	scanf("%lf", &p->amount);

	p->tx_time = date_current();
	if(strcmp(p->type, PAY_TYPE_FEES) == 0)
	// because of hardcore payment type
	//if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	else
		memset(&p->next_pay_duedate, 0, sizeof(date_t));
	//else
	//	memset(&p->next_pay_duedate, 0, sizeof(date_t));
	// Output #payment_display
	date_print(&p->next_pay_duedate);
}

// Output #payment_display
void payment_display(payment_t *p) {
	printf("Payment ID : %d\tMember ID : %d\tMemeber Type : %s\tAmount: %.2lf\n", p->id, p->memberid, p->type, p->amount);

	printf("payment ");
	// Logic #date_printing
	date_print(&p->tx_time);

	// Logic #due_date_printing 
	printf("payment due");
	date_print(&p->next_pay_duedate);
}


// Logic #autogenrated_payment_id
int get_next_payment_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t u;

	// Loogic #open_the_file
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;

	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Logic #mx_id
		max = u.id;
	fclose(fp);
	
	return max + 1;
}

//--------------------------------------------------

// Logic #genrating_next_user_id
int get_next_user_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;

	// Logic #open_file
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Logic #mx_id
		max = u.id;
	
	fclose(fp);

	return max + 1;
}

//---------------------------------------------------

// Logic #genrating_next_user_id
int get_next_book_id() {
	FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;

	// Logic #open_file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	
	// Logic #change_db_pointer_to_the_last_record
	fseek(fp, -size, SEEK_END);

	// Logic #read_record_from_db
	if(fread(&u, size, 1, fp) > 0)
		// Lgic #mx_id
		max = u.id;
	
	fclose(fp);

	return max + 1;
}

//--------------------------------------------------

// Logic #find_book_by_partial_name
void book_find_by_name(char name[]) {
	FILE *fp;
	int found = 0;
	book_t b;

	// Logic #open_file
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("failed to open books file");
		return;
	}

	// Logic #read_record_from_db_one_by_one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		// Logic #matching_name_partially
		if(strstr(b.name, name) != NULL) {
			found = 1;
			book_display(&b);
		}
	}
	
	fclose(fp);
	// Logic #nothing_found
	if(!found)
		printf("No such book found.\n");
}

//--------------------------------------------------

// Logic #Generic_edit_profile
void edit_profile(user_t *u){
	FILE *fp;
	user_t rec;
	
	// open users file
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open users file");
		exit(1);
	}
	// read users one by one and check if user with given id is found.
		while(fread(&rec, sizeof(user_t), 1, fp) > 0) {
			if(strcmp(rec.email, u->email) == 0){
				int size = sizeof(user_t);
				
				printf("name: ");
				scanf("%s", &u->name);

				printf("phone: ");
				scanf("%s", &u->phone);

				//Check the roles before edit
				if(strcmp(u->email,ROLE_LIBRARIAN) == 0)
					strcpy(u->role,ROLE_LIBRARIAN);
				else if(strcmp(u->email,ROLE_OWNER) == 0)
					strcpy(u->role,ROLE_OWNER);
				
				// File postition one record behind
				fseek(fp, -size , SEEK_CUR);
				
				//OverWrite Book Details
				fwrite(u, size ,1, fp);
				printf("Member Updated Successfully.....!\n");
				break;
			}
		}
		fclose(fp);
}

//--------------------------------------------------

// Logic #Generic_change_password
void change_password(user_t *u){
	FILE *fp;
	user_t m;
	char currPass[20],conPass[20];
	// open users file.
	fp = fopen(USER_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open users file\n");
		exit(1);
	}
	// read users one by one and check if member with given id is found.
	while(fread(&m, sizeof(user_t), 1, fp) > 0) {
		if(m.id == u->id) {
			printf("Current Password : ");
			scanf("%s",&currPass);
			if(strcmp(currPass,m.password) == 0){
				printf("NEW PASSWORD : ");
				scanf("%s",&m.password);
				printf("CONFIRM PASSWORD : ");
				scanf("%s",&conPass);
				if(strcmp(m.password, conPass) == 0){
					long size = sizeof(user_t);
					// File postition one record behind
					fseek(fp, -size , SEEK_CUR);
					user_display(&m);
					//Overwrite details in file
					fwrite(&m, size ,1, fp);
					printf("Password Updated Successfully....!");
				}else
					printf("Password didn't match....!");	
				break;
			}else
					printf("Password didn't match....!");	
				break;
		}
		}
		fclose(fp);
}

//--------------------------------------------------





