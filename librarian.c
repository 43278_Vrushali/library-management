#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

// Output #librarian_menu
void librarian_area(user_t *u) {
	// Declaration 
    int choice,memberid;
	char name[80];	

	// Logic #librarian_menu
	do {
        printf("************** LIBRARIAN : WELCOME %s **************",u->name);
		printf("\n\n0. Sign Out\n1. Add member\n2. Edit Profile\n3. Change Password\n4. Add Book\n5. Find Book\n6. Edit Book\n7. Check Availability\n8. Add Copy\n9. Change Rack\n10. Issue Copy\n11. Return Copy\n12. Take Payment\n13. Payment History\n");
		printf("14.Display Users\n15.Display Books\n16.Show Book Copies\n17.Show Issue Records\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // add member
				add_member();
				break;
			case 2: // Edit Profile
				edit_profile(u);
				break;
			case 3:// Change Password
				change_password(u);
				break;
			case 4: // add book 
			    add_book();
				break;
			case 5: // find book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 6: // edit book
				book_edit_id();
				break;
			case 7: // check avalibility
				bookcopy_checkavail_details();
				break;
			case 8: // add copy
				bookcopy_add();
				break;
			case 9:// Change Rack
				change_rack();
				break;
			case 10: // Issue Book Copy
				bookcopy_issue();
				break;
			case 11: // Return Book Copy
				bookcopy_return();
				break;
			case 12:// Take Payment
				fees_payment_add();
				break;
			case 13:// Payment History
				printf("enter member id of the member: ");
				scanf("%d", &memberid);
				payment_history(memberid);
				break;
			case 14:// Display Users
				view_all_users();
				break;
			case 15:// Display Books
				view_all_books();
				break;
			case 16:// Show Book Copies
				view_all_book_copies();	
				break;
			case 17://Show Issue Records
				view_all_issue_record();
				break;
		}
	}while (choice != 0);		
}

// Logic #appoint_member
void add_member() {
	// input #member_details
	user_t u;
	user_accept(&u);

	// Logic add_librarian_into_db
	user_add(&u);
}

// Logic #add_book
void add_book() {
	FILE *fp;
	// input #book_details
	book_t b;
	book_accept(&b);
	b.id = get_next_book_id();
	// Logic #add_book_to_db
	 // Logic open_books_db
	  fp = fopen(BOOK_DB, "ab");
	  if(fp == NULL) {
	    	perror("cannot open books file");
		    exit(1);
	    }
	// Logic #append_book_to_db
	fwrite(&b, sizeof(book_t), 1, fp);
	printf("book added in file.\n");
	// close books file.
	fclose(fp);
}

// Logic #edit_by_id
void book_edit_id() {
	int id, found = 0;
	FILE *fp;
	book_t b;

	// input #book_id
	printf("enter book id: ");
	scanf("%d", &id);

	// Logic #open_book
	fp = fopen(BOOK_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open books file");
		exit(1);
	}

	// Logic #read_record_one_by_one
	while(fread(&b, sizeof(book_t), 1, fp) > 0) {
		if(id == b.id) {
			found = 1;
			break;
		}
	}
	
	if(found) {
		// input #new_book_details 
		long size = sizeof(book_t);
		book_t nb;
		book_accept(&nb);
		nb.id = b.id;

		// Logic #db_pointer_behind_read_record
		fseek(fp, -size, SEEK_CUR);
		// Logic #overwrite_db
		fwrite(&nb, size, 1, fp);
		printf("book updated.\n");
	}
	else 
		printf("Book not found.\n");
	fclose(fp);
}

// Logic #bookcopy_add
void bookcopy_add() {
	FILE *fp;

	// input #book_copy_details
	bookcopy_t b;
	bookcopy_accept(&b);
	b.id = get_next_bookcopy_id();
	
	// Logic #add_book_copy_into_db
	fp = fopen(BOOKCOPY_DB, "ab");
	if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
	}

	// Logic #append_bookcopy_into_db.
	fwrite(&b, sizeof(bookcopy_t), 1, fp);
	printf("book copy added in file.\n");


	fclose(fp);
}

// Logic #bookcopy_ availability_details
void bookcopy_checkavail_details() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;

	// input #book_id
	printf("enter the book id: ");
	scanf("%d", &book_id);

	// Logic #open_bookcopies_db
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	// Logic #read_bookcopy_records_one_by_one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// Logic #afetr_checking_avalibility_print_bookcopy_details
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
			bookcopy_display(&bc);
			count++;
		}
	}
	fclose(fp);

	// Logic #no_bookcopy_available 
	if(count == 0)
		printf("no copies availables.\n");
}

// Logic #bookcopy_issue
void bookcopy_issue() {
	issuerecord_t rec;
	FILE *fp;
	
	// Input #issuerecord_details 
	issuerecord_accept(&rec);

	// if user is not paid, give error & return.
	if(!is_paid_member(rec.memberid)) {
		printf("member is not paid.\n");
		return;
	}

	// generate & assign new id for the issuerecord
	rec.id = get_next_issuerecord_id();

	// Logic open_issuerecord_db
	fp = fopen(ISSUERECORD_DB, "ab");
	if(fp == NULL) {
		perror("issuerecord file cannot be opened");
		exit(1);
	}

	// Logic #append_record_into_db
	fwrite(&rec, sizeof(issuerecord_t), 1, fp);
	fclose(fp);

	printf("Expected Return date : ",&rec.return_duedate);
	// Logic #mark_the_copy_as_issued
	bookcopy_changestatus(rec.copyid, STATUS_ISSUED);
}

// Logic #change_bookcopy_update_status
void bookcopy_changestatus(int bookcopy_id, char status[]) {
	bookcopy_t bc;
	FILE *fp;
	long size = sizeof(bookcopy_t);

	// Logic #open_bookcopy_db
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		return;
	}

	// Logic #read_bookcopy_one_by_one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		if(bookcopy_id == bc.id) {
			// Logic #modify_status
			strcpy(bc.status, status);
			// Logic #one_record_behind
			fseek(fp, -size, SEEK_CUR);
			// Logic #overwrite_bookcopy_status
			fwrite(&bc, sizeof(bookcopy_t), 1, fp);
			break;
		}
	}
	fclose(fp);
}

// Output #issued_book_of_member
void display_issued_bookcopies(int member_id) {
	FILE *fp;
	issuerecord_t rec;
	
	// Logic #open_issue_records_db
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}

	// Logic #read_records_one_by_one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {
		// Output #display
		if(rec.memberid == member_id && rec.return_date.day == 0)
			issuerecord_display(&rec);
	}
	fclose(fp);
}

// Logic #book_return
void bookcopy_return() {
	int member_id, record_id;
	FILE *fp;
	issuerecord_t rec;
	int diff_days, found = 0;
	long size = sizeof(issuerecord_t);

	// input #member_id
	printf("enter member id: ");
	scanf("%d", &member_id);

	// Output #issued_book_of_member
	display_issued_bookcopies(member_id);

	// Input #issue_record_id 
	printf("enter issue record id (to return): ");
	scanf("%d", &record_id);

	// Output #open_issuerecord_db
	fp = fopen(ISSUERECORD_DB, "rb+");
	if(fp==NULL) {
		perror("cannot open issue record file");
		return;
	}
	// Logic read_records_one_by_one
	while(fread(&rec, sizeof(issuerecord_t), 1, fp) > 0) {

		// Logic #find_issuerecord_id
		if(record_id == rec.id) {
			found = 1;
			// Logic #initialize_return_date
			rec.return_date = date_current();

			// Logic #check_fine
			diff_days = date_cmp(rec.return_date, rec.return_duedate);
			// Logic #update_fine_amount
			if(diff_days > 0) {
				rec.fine_amount = diff_days * FINE_PER_DAY;
				fine_payment_add(rec.memberid, rec.fine_amount);
				printf("fine amount Rs. %.2lf/- is applied for late return of %d days.\n", rec.fine_amount,diff_days);
			}
			break;
		}
	}
	
	// Logic #if_found
	if(found) {
		// Logic #record back
		fseek(fp, -size, SEEK_CUR);

		// Logic #overwrite_issue_record
		fwrite(&rec, sizeof(issuerecord_t), 1, fp);

		// Output #updated_issue_record.
		printf("issue record updated after returning book:\n");
		issuerecord_display(&rec);

		// Logic #update_copy_stats
		bookcopy_changestatus(rec.copyid, STATUS_AVAIL);
	}
	fclose(fp);
}

// Logic #fees_payment_add
void fees_payment_add() {
	FILE *fp;

	// Input #fees_payment
	payment_t pay;
	payment_accept(&pay);
	pay.id = get_next_payment_id();

	// Logic #open_db
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}

	// Logc #append_payment_data_into_the_db
	fwrite(&pay, sizeof(payment_t), 1, fp);

	fclose(fp);
}

// Logic #payment_history_of_member
void payment_history(int memberid) {
	FILE *fp;
	payment_t pay;

	// Logic #open_db
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return;
	}

	// Logic #read_payment_one_by_one_till_eof
	while(fread(&pay, sizeof(payment_t), 1, fp) > 0) {
		if(pay.memberid == memberid)
			payment_display(&pay);
	}

	fclose(fp);
}

// Logic #is_member_at_current_date
int is_paid_member(int memberid) {
	date_t now = date_current();
	FILE *fp;
	payment_t pay;
	int paid = 0;

	// Logic #open_db
	fp = fopen(PAYMENT_DB, "rb");
	if(fp==NULL) {
		perror("cannot open payment file");
		return 0;
	}
	// Logic #read_payment_one_by_one_till_eof
	while(fread(&pay, sizeof(payment_t), 1, fp) > 0) {
		if(pay.memberid == memberid && pay.next_pay_duedate.day != 0 && date_cmp(now, pay.next_pay_duedate) < 0) {
			paid = 1;
			break;
		}
	}
		
	fclose(fp);
	return paid;
}

// Logic #fine_payment_add
void fine_payment_add(int memberid, double fine_amount) {
	FILE *fp;

	// initialize #fine_payment
	payment_t pay;
	pay.id = get_next_payment_id();
	pay.memberid = memberid;
	pay.amount = fine_amount;
	strcpy(pay.type, PAY_TYPE_FINE);
	pay.tx_time = date_current();
	memset(&pay.next_pay_duedate, 0, sizeof(date_t));

	// Logic #open_db
	fp = fopen(PAYMENT_DB, "ab");
	if(fp == NULL) {
		perror("cannot open payment file");
		exit(1);
	}
	// Logic #append_payment_data_at_the_end_of_file
	fwrite(&pay, sizeof(payment_t), 1, fp);
	
	fclose(fp);
}

// Logic #Change Rack
void change_rack(){
	FILE *fp;
	bookcopy_t b;
	int found=0,change_rack_copy_id;
	 // open book copies file.
	fp = fopen(BOOKCOPY_DB, "rb+");
	if(fp == NULL) {
		perror("cannot open book copies file");
		exit(1);
	}

	//Get id
	printf("Enter CopyId to change the rack : ");
	scanf("%d",&change_rack_copy_id);
	while(fread(&b,sizeof(b), 1, fp) > 0){
		if(b.id == change_rack_copy_id){
			found =1;
			break;
		}
	}

	if(found){
		int size = sizeof(bookcopy_t);
		printf("Enter new Rack Id : ");
		scanf("%d",&b.rack);

		fseek(fp, -size , SEEK_CUR);
		//Overwrite book details
		fwrite(&b,size,1,fp);
		printf("Rack id updated successfully....!");
	}
	else
		printf("Copy Id not found....!\n");
	fclose(fp);
	
 }

//  Logic #view_all_books
void view_all_books(){
	FILE *fp;
	book_t b;
	// open books file.
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) {
		perror("cannot open books file\n");
		exit(1);
	}
	while((fread(&b , sizeof(b),1, fp) > 0 ))
		book_display(&b);

	fclose(fp);
}

//Logic #view_all_book_copies
void view_all_book_copies(){
	FILE *fp;
	bookcopy_t c;
	// open books file.
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open Book copy file\n");
		exit(1);
	}
	while((fread(&c , sizeof(c),1, fp) > 0 ))
		bookcopy_display(&c);

	fclose(fp);
}

// Logic #Output_all_users
void view_all_users(){
	FILE *fp;
	user_t u;

	// Logic #open_db
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) {
		perror("cannot open users file\n");
		exit(1);
	}

	// Output #users
	while((fread(&u , sizeof(u),1, fp) > 0 ))
		user_display(&u);

	fclose(fp);
}

// Logic #view_all_issue_record
 void view_all_issue_record(){
	FILE *fp;
	issuerecord_t ir;
	// open issuerecord file.
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL) {
		perror("cannot open issuerecord file\n");
		exit(1);
	}
	printf("ISSUE RECORD DETAILS : ");
	while((fread(&ir , sizeof(ir),1, fp) > 0 ))
		issuerecord_display(&ir);

	fclose(fp);
 }