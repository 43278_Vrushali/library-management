#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"

// Output #member_menu
void member_area(user_t *u) {
	// Declaration
	int choice;
	char name[80];

	// Logic #member_menu
	do {
		printf("************** MEMBER : WELCOME %s **************",u->name);
		printf("\n\n0. Sign Out\n1. Find Book\n2. Edit Profile\n3. Change Password\n4. Book Availability\n5.Issued Book\nEnter choice: ");
		scanf("%d", &choice);
		switch(choice) {
			case 1: // find book
				printf("Enter book name: ");
				scanf("%s", name);
				book_find_by_name(name);
				break;
			case 2:// Edit Profile
				edit_profile(u);
				break;
			case 3:// Change Password
				change_password(u);
				break;
			case 4: // Book Availability
				bookcopy_checkavail();
				break;
			case 5 : // List Issued Book
				display_issued_bookcopies(u->id);
				break;
		}
	}while (choice != 0);	
}

// Logic #check_book_avalibilty
void bookcopy_checkavail() {
	int book_id;
	FILE *fp;
	bookcopy_t bc;
	int count = 0;

	// Logic #input_book_id
	printf("enter the book id: ");
	scanf("%d", &book_id);
	// Logic open_bookcopies_db
	fp = fopen(BOOKCOPY_DB, "rb");
	if(fp == NULL) {
		perror("cannot open bookcopies file.");
		return;
	}

	// Logic #read_bookcopy_records_one_by_one
	while(fread(&bc, sizeof(bookcopy_t), 1, fp) > 0) {
		// Logic #afetr_checking_avalibility_print_bookcopy_details
		if(bc.bookid == book_id && strcmp(bc.status, STATUS_AVAIL)==0) {
			//bookcopy_display(&bc);
			count++;
		}
	}
	fclose(fp);
	// Output #avalibility_count
	printf("number of copies availables: %d\n", count);
}
