#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "library.h"
#include "date.h"

// Declartion_functions

void date_tester() {
	date_t d1 = {1, 1, 2000}, d2 = {31, 12, 2000};
	date_t d = {10, 10, 2000};
	date_t r = date_add(d, 10);
	date_print(&r);
	//int diff = date_cmp(d1, d2);
	//printf("date diff: %d\n", diff);
}

void tester() {
	user_t u;
	book_t b;
	user_accept(&u);
	user_display(&u);
	book_accept(&b);
	book_display(&b);
}

void sign_in() {
	// find the user in the users file by email 
	// check input password is correct.
	// if correct, call user_area() based on its role.

    // Declaration
    char email[30], password[10];
	user_t u;
	int invalid_user = 0;

	// input #email and password
	printf("email: ");
	scanf("%s", email);
	printf("password: ");
	scanf("%s", password);

	// Logic #finding_user_in_db_by_email
	if(user_find_by_email(&u, email) == 1) {
		// Logic #password_verification
		if(strcmp(password, u.password) == 0) {
			// Logic #owner_or_not
			if(strcmp(email, EMAIL_OWNER) == 0)
				strcpy(u.role, ROLE_OWNER);

			// Logic #call_user_area()_based_on_role.
			if(strcmp(u.role, ROLE_OWNER) == 0)
				owner_area(&u);
			else if(strcmp(u.role, ROLE_LIBRARIAN) == 0)
				librarian_area(&u);
			else if(strcmp(u.role, ROLE_MEMBER) == 0)
				member_area(&u);
			else
				invalid_user = 1;
		}
		else
			invalid_user = 1;
	}
	else
		invalid_user = 1;

	if(invalid_user)
		printf("Invalid email, password or role.\n");
}

void sign_up() {
	// input user details from the user.

     // Testing #adding_user_to_db
       user_t u;
       user_accept(&u);

	 //write user details into the users db.
      // Testing #add_user_in_db
       user_add(&u);
}


int main(){
    // Testing #program_is_runnung
    //printf("Good morning.\n");
 
     // Testing #input/output of book and user
     //tester();

     // testing #next_id_function
     //printf("new id :%d\n",get_next_user_id());

    // Declaration
    int choice;
	
    // Output #main_menu
	do {
		printf("\n\n0. Exit\n1. Sign In\n2. Sign Up\nEnter choice: ");
		scanf("%d", &choice);
		switch (choice)
		{
		case 1: // Sign In
			sign_in();
			break;
		case 2:	// Sign Up
			sign_up();	
			break;
		}
	}while(choice != 0);

    return 0;
}